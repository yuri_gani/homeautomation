var devices = {};
$(() => {
    $.material.init();
    const setDevices = function(devs) {
        devices = devs;
        var targetContainerEl = $('#devices');
        for (var deviceId in devices) {
            if (devices.hasOwnProperty(deviceId)) {
                var device = devices[deviceId];
                var checkbox = device.state.on ? 'checked="true"' : '';
                if (device.modelid == 'LCT007') {
                    var el = $(`
                    <div class="col-lg-6 col-sm-12 device" data-id="${deviceId}">
                        <div class="panel">
                            <div class="container-fluid">
                                <div class="panel-head">
                                    <h3 class="title">${device.name}</h3>
                                </div>
                                
                                <form>
                                    <div class="togglebutton">
                                        <label>
                                            ON &nbsp;&nbsp;
                                            <input class="state-on" type="checkbox" ${checkbox} data-toggle="toggle">
                                        </label>
                                    </div>
                                    <div class="form-group" hidden>
                                        <label>Brightness</label>
                                        <div class="slider shor slider-material-orange bri"></div>
                                    </div>
                                    <div class="form-group" hidden>
                                        <label>HUE</label>
                                        <div class="slider shor slider-material-orange hue"></div>
                                    </div>
                                    <div class="form-group" hidden>
                                        <label>Saturation</label>
                                        <div class="slider shor slider-material-orange sat"></div>
                                    </div>
                                    <div id="color_picker_${deviceId}" class="color-picker"></div>
                                </form>
                            </div>
                        </div>
                    </div>`);
                    el.appendTo(targetContainerEl);

                    var hsl = hsb2hsl(device.state.hue / 65535, device.state.sat / 254, device.state.bri / 254);
                    var hsls = `hsl(${hsl.h * 360}, ${hsl.s*100}%, ${hsl.l*100}%)`;
                    el.find('.title').css({
                        'color': hsls
                    });

                    var colorPicker = $(`#color_picker_${deviceId}`).colorpicker({
                        color: hsls,
                        container: true,
                        inline: true
                    });

                    let tempColor = {};
                    colorPicker.on('changeColor', function(event) {
                        var deviceEl = $(this).closest('.device');
                        var color = $(this).data('colorpicker').color.toHSL();
                        tempColor = color;
                        setTimeout(function() {
                            // console.log("color", color, tempColor);
                            if (color == tempColor) {

                                var hsls = `hsl(${color.h * 360}, ${color.s*100}%, ${color.l*100}%)`;
                                deviceEl.find(".title").css({
                                    'color': hsls
                                });

                                var hsb = hsl2hsb(color.h, color.s, color.l);
                                var hsvToSend = {
                                    hue: hsb.h * 65535,
                                    bri: hsb.b * 254,
                                    sat: hsb.s * 254
                                }
                                console.log('color change', hsvToSend);


                                setDeviceState(deviceEl.attr('data-id'), Object.assign(device.state, hsvToSend));
                            }
                        }, 400);
                    });
                    var bri = el.find(".shor.bri").noUiSlider({
                        start: device.state.bri,
                        connect: "lower",
                        range: {
                            min: 0,
                            max: 254
                        }
                    });
                    el.find(".shor.hue").noUiSlider({
                        start: device.state.hue,
                        connect: "lower",
                        range: {
                            min: 0,
                            max: 65535
                        }
                    });
                    el.find(".shor.sat").noUiSlider({
                        start: device.state.sat,
                        connect: "lower",
                        range: {
                            min: 0,
                            max: 254
                        }
                    });
                    el.find(".shor").on("slide", function(event) {
                        var slider = $(this);
                        var panel = slider.closest('.device');
                        var deviceId = panel.attr('data-id');
                        var device = devices[deviceId];
                        var cls = '';
                        var state = {
                            hue: device.state.hue,
                            bri: device.state.bri,
                            sat: device.state.sat
                        };
                        if (slider.hasClass('bri')) {
                            cls = 'bri';

                        } else if (slider.hasClass('sat')) {
                            cls = 'sat';
                        } else if (slider.hasClass('hue')) {
                            cls = 'hue';
                        }
                        state[cls] = slider.val();

                        var hsl = hsb2hsl(state.hue / 65535, state.sat / 254, state.bri / 254);
                        var hsls = `hsl(${hsl.h * 360}, ${hsl.s*100}%, ${hsl.l*100}%)`;
                        panel.find('.title').css({
                            'color': hsls
                        });
                    });

                    el.find(".shor").on("change", function(event) {
                        var slider = $(this);
                        var deviceId = slider.closest('.device').attr('data-id');
                        var device = devices[deviceId];
                        if (slider.hasClass("bri")) {
                            setDeviceState(deviceId, Object.assign(device.state, {
                                bri: parseFloat(slider.val())
                            }))
                        } else if (slider.hasClass("hue")) {
                            setDeviceState(deviceId, Object.assign(device.state, {
                                hue: parseFloat(slider.val())
                            }))
                        } else if (slider.hasClass("sat")) {
                            setDeviceState(deviceId, Object.assign(device.state, {
                                sat: parseFloat(slider.val())
                            }))
                        }
                    });
                    el.find('.state-on').on('change', function(event) {
                        var deviceId = $(event.currentTarget).closest('.device').attr('data-id');
                        setDeviceState(deviceId, Object.assign(device.state, {
                            on: $(event.currentTarget).prop('checked')
                        }));
                    });

                } else {
                    var el = $(`
                    <div class="col-lg-6 col-sm-12" id="${deviceId}">
                        <div class="panel">
                            <div class="container-fluid">
                                <div class="panel-head">
                                    <h3 class="title">${device.name}</h3>
                                </div>
                            </div>
                        </div>
                    </div>`);
                    el.appendTo(targetContainerEl);
                }

            }
        }
        $.material.init();
    }
    $.ajax({
        url: '/getDevices',
        type: 'get',
        success: function(response) {
            setDevices(response);
        },
        failed: function(err, response) {
            console.error(err, response);
        }
    })
})

var setDeviceState = function(deviceId, state) {
    $.ajax({
        type: 'PUT',
        data: state,
        url: '/device/' + deviceId,
        success: function(response) {
            console.log(deviceId, state);
            devices[deviceId].state = Object.assign(devices[deviceId].state, state);
        },
        failed: function(err, response) {
            console.error(err, response);
        }
    })
}

function hsb2hsl(hue, sat, bri) {
    var h = hue;
    var l = bri > 0 ? bri * (2 - sat) / 2 : 0;
    var s = bri * sat > 0 ? bri * sat / (1 - Math.abs((2 * l) - 1)) : 0;
    return {
        h,
        s,
        l
    };
}

function hsl2hsb(hue, sat, lig) {
    var h = hue;
    var b = lig + sat * (1 - Math.abs((2 * lig) - 1)) / 2
    var s = 2 * (b - lig) / b;
    // var l = bri > 0 ? bri * (2 - sat) / 2 : 0;
    // var s = bri * sat > 0 ? bri * sat / (1 - Math.abs((2 * l) - 1)) : 0;
    return {
        h,
        s,
        b
    };
}