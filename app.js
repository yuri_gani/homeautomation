var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cons = require('consolidate');

require('body-parser-xml')(bodyParser);

var MeethuePhilip = require('./libs/MeethuePhilip');

var users = require('./routes/users');
var index = require('./routes/index');
var mnListener = require('./routes/mnListener');

// view engine setup

var app = express();

// view engine setup
app.engine('html', cons.swig);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(bodyParser.xml());
app.use(bodyParser.urlencoded({ extended: false }));


// var mnListener = function(req, res, next) {
//     req.body
//     var xml = req.body['m2m:sgn'].nev[0].rep[0].con[0];
//     var ins = util.inspect(xml, false, null);
//     ins = ins.replace("\n", ' ');

//     ins = ins.replace(/(\\n)*\s+/g, ' ').trim();
//     ins = ins.replace(/^'|'$/g, '');
//     // console.log(ins);
//     parseString(ins, function(err, result) {
//         if (err)
//             throw new Error(err);
//         // console.log(err, result.obj.int);
//         var is = result.obj.int;
//         console.log(result.obj);
//         var state = {

//         }

//         var ids = [];
//         for (i in is) {
//             obj = is[i]['$'];
//             if (obj.name == "status") {
//                 state.on = (is[i]['$'].val == 'true');
//             } else if (obj.name == "bri") {
//                 state.bri = parseInt(obj.val);
//             } else if (obj.name == "hue") {
//                 state.hue = parseInt(obj.val);
//             } else if (obj.name == "sat") {
//                 state.sat = parseInt(obj.val);
//             } else if (obj.name == "id") {
//                 ids.push(obj.val);
//             }
//         }
//         var options = {
//             method: 'PUT',
//             // url: 'http://192.168.100.14/api/OONd4uKaBa0N8eocjMLtEmFGxK5T53KmzrIGMXlG/lights/2/state',
//             headers: {
//                 'content-type': 'application/json'
//             },
//             body: state,
//             json: true
//         };
//         console.log('state', state);
//         ids.forEach(function(id) {
//             MeethuePhilip.meethue_gateway_url_bases.forEach(function(base) {
//                 options.url = base + MeethuePhilip.meethue_gateway_url_path + '/' + id + '/state';
//                 console.log("url", options.url);
//                 request(options, function(error, response, body) {
//                     if (error) throw new Error(error);

//                     console.log(body);
//                 });
//             }, this)

//         }, this);
//     })
//     res.send('ha!');
// }

app.use('/home', index);
app.use('/monitor', mnListener);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    console.log('not found', req.path);
    if (req.path.startsWith('/home')) {
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
    } else {
        index(req, res, next);
        // res.redirect('/home' + req.path)
    }

});

// error handler
app.use(function(err, req, res, next) {
    console.log(req.path, err);
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});
MeethuePhilip.loadMeethueGateways((err, gateways) => {
    console.log(err, gateways);
});



// MeethuePhilip.loadMeethueGateways((err, gateways) => {
//     if (err) {
//         console.error(err);
//         return;
//     }
//     console.log(gateways);
//     gateways.forEach(function(gw) {
//         MeethuePhilip.getMeethueDevices(gw, function(err, response) {
//             if (err) {
//                 console.error(err);
//                 return;
//             }
//             // console.log(err, response);
//             for (var light in response) {
//                 if (response.hasOwnProperty(light)) {
//                     var state = response[light];
//                     MeethuePhilip.getMeethueStatus(gw, light, (err, response) => {
//                         if (err) {
//                             console.log('light' + light, err);
//                             return;
//                         }
//                         console.log('light' + light, response.state);
//                     });
//                 }
//             }
//         });
//     }, this);
// });

module.exports = app;