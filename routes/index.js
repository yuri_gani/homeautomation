var express = require('express');
var router = express.Router();

var MeethuePhilip = require('../libs/MeethuePhilip');

var config = require('../config');

/* GET home page. */
router.get('/', function(req, res, next) {
    MeethuePhilip.getMeethueDevices(MeethuePhilip.meethue_gateway_url_bases[0], (err, devices) => {
        console.log(err, devices);
        res.render('index', { title: 'HomeAutomation', devices: JSON.stringify(devices) });
    })
});

router.get('/getDevices', function(req, res, next) {
    if (MeethuePhilip.meethue_gateway_url_bases.length == 0) {
        MeethuePhilip.loadMeethueGateways((err, gateways) => {
            console.log(err, gateways);
            MeethuePhilip.getMeethueDevices(MeethuePhilip.meethue_gateway_url_bases[0], (err, devices) => {
                if (err) {
                    console.log(err, devices);
                }
                res.send(devices);
            })
        })
    } else {
        MeethuePhilip.getMeethueDevices(MeethuePhilip.meethue_gateway_url_bases[0], (err, devices) => {
            console.log(err, devices);
            res.send(devices);
        })

    }
});

router.put('/device/:id', function(req, res, next) {
    console.log(req.params.id, req.body);
    MeethuePhilip.setState(null, req.params.id, req.body);
    res.send(req.body);
});

module.exports = router;