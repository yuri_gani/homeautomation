var express = require('express');
var router = express.Router();
var parseString = require('xml2js').parseString;
var util = require('util');
var request = require('request');

var MeethuePhilip = require("../libs/MeethuePhilip.js");

/* GET home page. */
router.post('/', function(req, res, next) {
    if (!Array.isArray(req.body['m2m:sgn'].nev)) {
        console.log('body', req.body);
        res.send("{}");
        return;
    }
    var xml = req.body['m2m:sgn'].nev[0].rep[0].con[0];
    var ins = util.inspect(xml, false, null);
    ins = ins.replace("\n", ' ');

    ins = ins.replace(/(\\n)*\s+/g, ' ').trim();
    ins = ins.replace(/^'|'$/g, '');
    // console.log(ins);
    parseString(ins, function(err, result) {
        if (err)
            throw new Error(err);
        // console.log(err, result.obj.int);
        var is = result.obj.int;
        console.log(result.obj);
        var state = {

        }

        var ids = [];
        for (i in is) {
            obj = is[i]['$'];
            if (obj.name == "status") {
                state.on = (is[i]['$'].val == 'true');
            } else if (obj.name == "bri") {
                state.bri = parseInt(obj.val);
            } else if (obj.name == "hue") {
                state.hue = parseInt(obj.val);
            } else if (obj.name == "sat") {
                state.sat = parseInt(obj.val);
            } else if (obj.name == "id") {
                ids.push(obj.val);
            }
        }
        var options = {
            method: 'PUT',
            // url: 'http://192.168.100.14/api/OONd4uKaBa0N8eocjMLtEmFGxK5T53KmzrIGMXlG/lights/2/state',
            headers: {
                'content-type': 'application/json'
            },
            body: state,
            json: true
        };
        console.log('state', state);
        ids.forEach(function(id) {
            MeethuePhilip.meethue_gateway_url_bases.forEach(function(base) {
                options.url = base + MeethuePhilip.meethue_gateway_url_path + '/' + id + '/state';
                console.log("url", options.url);
                request(options, function(error, response, body) {
                    if (error) throw new Error(error);

                    console.log(body);
                });
            }, this)

        }, this);
    })
    res.send('ha!');
});

module.exports = router;