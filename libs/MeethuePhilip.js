var request = require('request');
var bodyParser = require('body-parser');
var parseString = require('xml2js').parseString;
require('body-parser-xml')(bodyParser);

var config = require('../config');
console.log(config);
if (typeof config.mngateway.subscribe)
    config.mngateway.subscribe = typeof config.mngateway.subscribe === 'undefined' ? true : config.mngateway.subscribe;


// var myIpAddress = '192.168.100.17';
// var myIpAddress = '192.168.100.18';
var myIpAddress = 'localhost';

console.log('ipaddresses', getMyIpAddress());

var connectedToMn = false;

function getMyIpAddress() {
    var os = require('os');
    var ifaces = os.networkInterfaces();

    var inames = Object.keys(ifaces);
    var retvals = {};
    for (var i = 0; i < inames.length; i++) {
        var iname = inames[i];
        for (var j = 0; j < ifaces[iname].length; j++) {
            var iface = ifaces[iname][j];
            if ('IPv4' !== iface.family || iface.internal !== false) {
                // skip IPv6 or internal
            } else {
                if (!retvals[iname]) {
                    retvals[iname] = []
                }
                retvals[iname].push(iface.address);
            }
        }
    }
    return retvals;
}

const MeethuePhilip = {
    meethue_gateway_url_bases: [],
    loadMeethueGateways: function(fn) {
        if (config.philiphue) {
            request({
                url: 'https://www.meethue.com/api/nupnp',
                method: 'GET',
                headers: {
                    'content-type': 'application/json'
                },
                json: true
            }, function(err, response, body) {
                if (err) {
                    if (typeof fn === 'function') {
                        fn(err, MeethuePhilip.meethue_gateway_url_bases);
                    } else {
                        console.error(err);
                    }
                    return;
                }
                MeethuePhilip.meethue_gateway_url_bases.splice(0, MeethuePhilip.meethue_gateway_url_bases.length);
                for (var i = 0; i < body.length; i++) {
                    MeethuePhilip.meethue_gateway_url_bases.push('http://' + body[i].internalipaddress);
                }
                if (typeof fn === 'function') {
                    fn(null, MeethuePhilip.meethue_gateway_url_bases);
                } else {
                    console.log(MeethuePhilip.meethue_gateway_url_bases);
                }

            });
        } else {
            fn(new Error("philip Hue Not initialized"));
        }
    },
    getMeethueDevices: function(gatewayUrl, fn) {
        if (config.philiphue) {
            gatewayUrl = gatewayUrl || MeethuePhilip.meethue_gateway_url_bases[0];
            var options = {
                method: 'GET',
                url: `${gatewayUrl}/api/${config.philiphue.uniqueId}/lights/`,
                headers: {
                    'content-type': 'application/json'
                },
                json: true
            };

            request(options, function(error, response, body) {
                if (error) {
                    console.error(error);
                    MeethuePhilip.loadMeethueGateways(function(err, gateways) {

                    });
                }
                if (typeof fn === 'function') {
                    fn(error, body);
                } else {
                    console.log(error, body);
                }
            });
        } else {
            fn(new Error("philip Hue Not initialized"), {
                '1': {
                    name: 'PhilipHue 1',
                    modelid: 'LCT007',
                    state: {
                        on: false,
                        bri: 0,
                        sat: 0,
                        hue: 0
                    },

                },
                '2': {
                    name: 'PhilipHue 2',
                    modelid: 'LCT007',
                    state: {
                        on: false,
                        bri: 0,
                        sat: 0,
                        hue: 0
                    },

                },
                '3': {
                    name: 'PhilipHue 3',
                    modelid: 'LCT007',
                    state: {
                        on: false,
                        bri: 0,
                        sat: 0,
                        hue: 0
                    },

                }
            });
        }

    },
    getMeethueStatus: function(gatewayUrl, light, fn) {
        if (config.philiphue) {
            gatewayUrl = gatewayUrl || MeethuePhilip.meethue_gateway_url_bases[0];
            light = light || 1;
            var options = {
                method: 'GET',
                url: `${gatewayUrl}/api/${config.philiphue.uniqueId}/lights/${light}`,
                headers: {
                    'content-type': 'application/json'
                },
                json: true
            };

            request(options, function(error, response, body) {
                if (typeof fn === 'function') {
                    fn(error, body);
                } else {
                    console.log(error, body);
                }
            });
        } else {
            fn(new Error("philip Hue Not initialized"));
        }

    },
    setState: function(gateway, light, state, fn) {
        var options = {
            method: 'POST',
            url: `${config.mngateway.protocol}://${config.mngateway.address}:${config.mngateway.port}/~/${config.mngateway.q}/${config.mngateway.r}${config.mngateway.s ? '/' + config.mngateway.s : '' }/${config.mngateway.project}/${config.mngateway.device}`,
            body: `<m2m:cin xmlns:m2m="http://www.onem2m.org/xml/protocols">
                <cnf>message</cnf>
                <con> &lt;obj&gt;
                    &lt;str name=&quot;category&quot; val=&quot;temperature&quot;/&gt;
                    &lt;int name=&quot;id&quot; val=&quot;${light}&quot;/&gt;
                    &lt;int name=&quot;status&quot; val=&quot;${state.on}&quot;/&gt;
                    &lt;int name=&quot;bri&quot; val=&quot;${state.bri}&quot;/&gt;
                    &lt;int name=&quot;hue&quot; val=&quot;${state.hue}&quot;/&gt;
                    &lt;int name=&quot;sat&quot; val=&quot;${state.sat}&quot;/&gt;
                    &lt;int name=&quot;unit&quot; val=&quot;boolean&quot;/&gt;
                    &lt;/obj&gt;
                </con>
            </m2m:cin>`,
            headers: {
                'content-type': 'application/xml;ty=4',
                'x-m2m-origin': 'iot:iottelkom'
            }
        }
        console.log('setstate', options.url);
        request(options, function(error, response, body) {
            console.log(error, body);
            if (error) {
                MeethuePhilip.loadMeethueGateways(function(err, response) {
                    console.log(err, response);
                })
            }
            if (typeof fn === 'function') {
                fn(error, body);
            } else {
                // console.log(error, body);
            }
        });
    },
    init: function() {

        if (config.mngateway.subscribe) {
            MeethuePhilip.meethue_gateway_url_path = `/api/${config.philiphue.uniqueId}/lights`;
            if (!connectedToMn) {
                console.log("init");
                addProjectIfNotExists(function(err) {
                    if (err) {
                        console.error(err);
                        return;
                    }
                    addDeviceNotExists(function(err) {
                        if (err) {
                            console.error(err);
                            return;
                        }
                        addSubscriberIfNotExist(function(err) {
                            if (err) {
                                console.error(err);
                                return;
                            }
                            connectedToMn = true;
                        });
                    });
                })
                setTimeout(function() {
                    MeethuePhilip.init();
                }, 60000);
            }
        }
    }
};



var addProjectIfNotExists = function(fn) {
    var getOptions = {
        method: 'GET',
        url: `${config.mngateway.protocol}://${config.mngateway.address}:${config.mngateway.port}/~/${config.mngateway.q}?fu=1&ty=1`,
        headers: {
            'content-type': 'application/json;ty=2',
            'x-m2m-origin': 'iot:iottelkom'
        },
        body: `<m2m:ae xmlns:m2m="http://www.onem2m.org/xml/protocols" rn="${config.mngateway.project}" >
                <api>app-sensor</api>
                <username>ale</username>
                <lbl>Location/home</lbl>
                <rr>false</rr>
            </m2m:ae>`
    };

    request(getOptions, function(error, response, body) {
        if (error) {
            console.error('url', getOptions.url);
            fn(error, body);
            return;
        }
        if (typeof body === 'string') {
            console.log('addProjectIfNotExists', body);
            body = JSON.parse(body);
        }
        console.log(getOptions.url, body["m2m:uril"]);
        if (body["m2m:uril"].indexOf(`/${config.mngateway.q}/${config.mngateway.r}/acpae`) > -1) {
            console.log(`${config.mngateway.project} already exists`);
            fn(error);
        } else {
            var postOpt = {
                method: 'POST',
                url: `${config.mngateway.protocol}://${config.mngateway.address}:${config.mngateway.port}/~/${config.mngateway.q}`,
                headers: {
                    'content-type': 'application/xml;ty=2',
                    'x-m2m-origin': 'iot:iottelkom'
                },
                body: `<m2m:ae xmlns:m2m="http://www.onem2m.org/xml/protocols" rn="${config.mngateway.project}" >
                    <api>app-sensor</api>
                    <username>ale</username>
                    <lbl>Location/home</lbl>
                    <rr>false</rr>
                </m2m:ae>`
            };

            request(postOpt, function(error, response, body) {
                if (error) {
                    console.error(error);
                    fn(error, null);
                    return;
                }
                fn(error);
            });
        }
    });
}

var addDeviceNotExists = function(fn) {
    var options = {
        method: 'GET',
        url: `${config.mngateway.protocol}://${config.mngateway.address}:${config.mngateway.port}/~/${config.mngateway.q}/${config.mngateway.r}/${config.mngateway.project}`,
        qs: { fu: '1', ty: '3' },
        headers: {
            accept: 'application/json',
            'x-m2m-origin': 'iot:iottelkom'
        }
    };

    request(options, function(error, response, body) {
        if (error) {
            fn(error, null);
            return;
        }

        if (response.statusCode == 404) { // not exists
            console.log(response.statusCode, body);
            var options = {
                method: 'POST',
                url: `${config.mngateway.protocol}://${config.mngateway.address}:${config.mngateway.port}/~/${config.mngateway.q}/${config.mngateway.r}/${config.mngateway.project}`,
                headers: {
                    'content-type': 'application/json;ty=3',
                    'x-m2m-origin': 'iot:iottelkom'
                },
                body: `<m2m:cnt xmlns:m2m="http://www.onem2m.org/xml/protocols" rn="${config.mngateway.device}"></m2m:cnt>`
            };

            request(options, function(error, response, body) {
                if (error) {
                    console.error(error);
                    fn(error, null);
                    return;
                }

                console.log(body);
                fn(error, body);
            });
        } else {

            if (typeof body === 'string') {
                body = JSON.parse(body);
            }
            if (typeof body['m2m:uril'] !== 'string') {
                console.log('m2m:uril', body['m2m:uril']);
                createDevice();
            } else if (body['m2m:uril'].indexOf(config.mngateway.device) > -1) {
                console.log(`${config.mngateway.device} already Exists`);
                fn(error);
            } else {
                createDevice();
            }
        }

        function createDevice() {
            var options = {
                method: 'POST',
                url: `${config.mngateway.protocol}://${config.mngateway.address}:${config.mngateway.port}/~/${config.mngateway.q}/${config.mngateway.r}/${config.mngateway.project}`,
                headers: {
                    'content-type': 'application/xml;ty=3',
                    'x-m2m-origin': 'iot:iottelkom'
                },
                body: `<m2m:cnt xmlns:m2m="http://www.onem2m.org/xml/protocols" rn="${config.mngateway.device}">\n</m2m:cnt>`
            };

            request(options, function(error, response, body) {
                if (error) {
                    console.error(error);
                    fn(error, null);
                    return;
                }

                console.log(body);
                fn(error);
            });
        }
    });




}

var addSubscriberIfNotExist = function(fn) {
    var request = require("request");

    var options = {
        method: 'GET',
        url: `${config.mngateway.protocol}://${config.mngateway.address}:${config.mngateway.port}/~/${config.mngateway.q}/${config.mngateway.r}/${config.mngateway.project}/${config.mngateway.device}`,
        qs: { fu: '1', ty: '23' },
        headers: {
            accept: 'application/json',
            'x-m2m-origin': 'iot:iottelkom'
        }
    };

    request(options, function(error, response, body) {
        if (error) {
            console.error(error);
            fn(error, null);
            return;
        }

        if (typeof body === 'string') {
            body = JSON.parse(body);
        }
        console.log(body);

        if (typeof body['m2m:uril'] === 'string' && body['m2m:uril'].indexOf(myIpAddress) > -1) {
            console.log("already Subscribed");
            fn(error);
        } else {

            var options = {
                method: 'POST',
                url: `${config.mngateway.protocol}://${config.mngateway.address}:${config.mngateway.port}/~/${config.mngateway.q}/${config.mngateway.r}/${config.mngateway.project}/${config.mngateway.device}`,
                headers: {
                    'content-type': 'application/xml;ty=23',
                    'x-m2m-origin': 'iot:iottelkom'
                },
                body: `<m2m:sub xmlns:m2m="http://www.onem2m.org/xml/protocols" rn="SUB-${myIpAddress}">
                    <nu>http://${myIpAddress}:3000/monitor</nu>
                <nct>2</nct>\n</m2m:sub>`
            };

            request(options, function(error, response, body) {
                if (error) {
                    console.error(error);
                    fn(error, null);
                    return;
                }

                console.log(body);
                fn(error);
            });

        }
    });

}

console.log("MeethuePhilip");
// var meethue_gateway_url_bases = [];
// var meethue_gateway_url_path = `/api/${config.philiphue.uniqueId}/lights`;

// const loadMeethueGateways =
//     const getMeethueDevices =
//         const getMeethueStatus =



MeethuePhilip.init();
exports = module.exports = MeethuePhilip;